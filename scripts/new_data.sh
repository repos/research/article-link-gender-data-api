#!/usr/bin/env bash

# these can be changed but most other variables should be left alone
DATA_WGET='https://analytics.wikimedia.org/published/datasets/one-off/isaacj/gender-data/gender_all_2022_11_07.sqlite'

PY_PATH="${HOME}/www/python"  # where virtualenv will sit
REPO_PATH="${PY_PATH}/src"  # where the code / data will sit
DATA_FN="${REPO_PATH}/gender.sqlite"

echo "Downloading data, hang on..."
rm ${DATA_FN}
wget -O ${DATA_FN} ${DATA_WGET}

echo "Restarting webservice..."
webservice --backend=kubernetes python3.9 restart