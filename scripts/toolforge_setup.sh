#!/usr/bin/env bash
# setup Toolforge instance with initial data / Flask API
# to run:
# * ssh onto Toolforge instance `$ ssh isaacj@login.toolforge.org`
# * become the tool `$ become article-link-gender-data`
# * add this script to your home directory:
#   * `$ wget -O toolforge_setup.sh https://gitlab.wikimedia.org/repos/research/article-link-gender-data-api/-/raw/main/scripts/toolforge_setup.sh`
# * make executable: `$ chmod +x toolforge_setup.sh`
# * execute `$ ./toolforge_setup.sh`
# * setup Python virtualenv (https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web/Python#Virtual_Environments_and_Packages):
#   * `$ webservice --backend=kubernetes python3.9 shell`
#   * `$ webservice-python-bootstrap`
#   * `$ logout`
# * start webservice: `$ webservice --backend=kubernetes python3.9 start`
#
# For updating just the code, navigate to repo and pull in changes `$ git pull`
# Go through Python virtualenv setup again
# restart webservice: `$ webservice --backend=kubernetes python3.9 restart`

# these can be changed but most other variables should be left alone
GIT_CLONE_HTTPS='https://gitlab.wikimedia.org/repos/research/article-link-gender-data-api.git'  # for `git clone`
DATA_WGET='https://analytics.wikimedia.org/published/datasets/one-off/isaacj/gender-data/gender_all_2022_11_07.sqlite'

PY_PATH="${HOME}/www/python"  # where virtualenv will sit
REPO_PATH="${PY_PATH}/src"  # where the code / data will sit
DATA_FN="${REPO_PATH}/gender.sqlite"

echo "Setting up paths..."
rm -rf ${PY_PATH}
mkdir -p ${PY_PATH}
rm -rf ${REPO_PATH}

echo "Downloading data, hang on..."
wget -O ${DATA_FN} ${DATA_WGET}

echo "Cloning repositories..."
git clone ${GIT_CLONE_HTTPS} ${REPO_PATH}